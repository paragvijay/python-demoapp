FROM python:3.9-slim

LABEL Name="Python Flask Demo App" Version=1.4.2
LABEL org.opencontainers.image.source = "https://gitlab.com/paragvijay/python-demoapp"


COPY src /app/

WORKDIR /app

RUN pip install --no-cache-dir -r requirements.txt


EXPOSE 5000

CMD ["gunicorn", "-b", "0.0.0.0:5000", "run:app"]